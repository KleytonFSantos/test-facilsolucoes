
## Como iniciar o script

### Instale um pacote no qual permite utilizar ambientes de variáveis para armazenar dados sensíveis

```
composer install

```

### Adicione as seguintes variáveis de ambiente

```
DB_CONNECTION=mysql
DB_HOST=0.0.0.0
DB_PORT=3306
DB_DATABASE=nome_database
DB_USERNAME=username
DB_PASSWORD=password
```

### Após isso, rode o script normalmente da forma que preferir.
