<?php
require 'vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

try {
    $db_name = $_ENV['DB_DATABASE'];
    $db_host = $_ENV['DB_HOST'];
    $db_user = $_ENV['DB_USERNAME'];
    $db_pass = $_ENV['DB_DATABASE'];
    $conn = new PDO("mysql:host={$db_host};dbname={$db_name}", "{$db_user}", "{$db_pass}");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = "
            SELECT
                b.nome as nome_banco,
                cv.verba,
                c.codigo AS codigo_contrato,
                c.data_inclusao,
                c.valor,
                c.prazo
                FROM Tb_contrato c
                LEFT JOIN Tb_convenio as cv on cv.codigo = c.convenio_servico
                LEFT JOIN Tb_banco as b on b.codigo = cv.banco";

    $stmt = $conn->prepare($query);

    $stmt->execute();

    $result = $stmt->fetchAll();

    echo "<table>";
    echo "<tr><th>Nome do banco</th><th>Verba</th><th>Código do contrato</th><th>Data de inclusão</th><th>Valor</th><th>Prazo</th></tr>";

    foreach ($result as $row) {
        echo "<tr>";
        echo "<td>" . $row['nome_banco'] . "</td>";
        echo "<td>" . $row['verba'] . "</td>";
        echo "<td>" . $row['codigo_contrato'] . "</td>";
        echo "<td>" . $row['data_inclusao'] . "</td>";
        echo "<td>" . $row['valor'] . "</td>";
        echo "<td>" . $row['prazo'] . "</td>";
        echo "</tr>";
    }
    echo "</table>";
} catch (PDOException $e) {
    echo $db_name, $db_host, $db_user, $db_pass;

    echo "Falha: " . $e->getMessage();
}

$conn = null;
